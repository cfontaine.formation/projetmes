﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Projet_mes.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projet_mes.ControlViews
{
    public class MovieViewModel
    {
        public Movie Movie { get; set; }

        public IList<Genre> Genres { get; set; }

        public IList<Nation> Nations { get; set; }

        public IFormFile Poster { get; set; }
    }
}

