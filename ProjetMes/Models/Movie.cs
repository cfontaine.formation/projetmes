﻿using Projet_mes.Models;
using System.Collections.Generic;
using System.Xml.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projet_mes.Models
{
	[Table("movies")]
	public class Movie : AbstractEntity
	{
		[Required]
		[Column("title_vf")]
		public string? TitleVf { get; set; }

		[Column("title_vo")]
		public string? TitleVo { get; set; }

		[Required]
		public int Duration { get; set; }

		[Required]
		[Column("release_date")]
		public DateTime? ReleaseDate { get; set; }

		[Column("movie_poster")]
		public string? MoviePoster { get; set; }

		[Column("ident_video")]
		public string? IdentVideo { get; set; }

		public string? Summary { get; set; }

		public Nation? Nationality { get; set; }

		public Genre? Genre { get; set; }

		public ICollection<Participant> Realisators { get; set; }

		public ICollection<Participant> Actors { get; set; }
	}
}
