﻿using Projet_mes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;

namespace Projet_mes.Models
{
    [Table("genres")]
    public class Genre : AbstractEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}

