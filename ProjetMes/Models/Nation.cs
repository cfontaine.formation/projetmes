﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projet_mes.Models
{
	[Table("nations")]
	public class Nation : AbstractEntity
	{
		[Required]
		[MaxLength(100)]
		public string Name { get; set; }

		public ICollection<Participant> Actors { get; set; }

		public ICollection<Movie> Movies { get; set; }
	}
}
