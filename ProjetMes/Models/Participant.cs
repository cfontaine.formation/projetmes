﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projet_mes.Models
{
	[Table("participants")]
	public class Participant : AbstractEntity
	{
		[Required]
		[MaxLength(50)]
		public string? FirstName { get; set; }


		[MaxLength(50)]
		public string? LastName { get; set; }

		[Required]
		public DateTime? BirthDate { get; set; }

		public DateTime? DeathDate { get; set; }

		public Nation Nationality { get; set; }

		public ICollection<Movie> MoviesRealisators { get; set; }

		public ICollection<Movie> MoviesActors { get; set; }
	}
}
