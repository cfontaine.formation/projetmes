﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Projet_mes.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "genres",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_genres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "nations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "movies",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title_vf = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    title_vo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Duration = table.Column<int>(type: "int", nullable: false),
                    release_date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    movie_poster = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ident_video = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Summary = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NationalityId = table.Column<long>(type: "bigint", nullable: true),
                    GenreId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_movies_genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "genres",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_movies_nations_NationalityId",
                        column: x => x.NationalityId,
                        principalTable: "nations",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "participants",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeathDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    NationalityId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_participants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_participants_nations_NationalityId",
                        column: x => x.NationalityId,
                        principalTable: "nations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "actors",
                columns: table => new
                {
                    ActorsId = table.Column<long>(type: "bigint", nullable: false),
                    MoviesActorsId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_actors", x => new { x.ActorsId, x.MoviesActorsId });
                    table.ForeignKey(
                        name: "FK_actors_movies_MoviesActorsId",
                        column: x => x.MoviesActorsId,
                        principalTable: "movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_actors_participants_ActorsId",
                        column: x => x.ActorsId,
                        principalTable: "participants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "realisators",
                columns: table => new
                {
                    MoviesRealisatorsId = table.Column<long>(type: "bigint", nullable: false),
                    RealisatorsId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_realisators", x => new { x.MoviesRealisatorsId, x.RealisatorsId });
                    table.ForeignKey(
                        name: "FK_realisators_movies_MoviesRealisatorsId",
                        column: x => x.MoviesRealisatorsId,
                        principalTable: "movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_realisators_participants_RealisatorsId",
                        column: x => x.RealisatorsId,
                        principalTable: "participants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_actors_MoviesActorsId",
                table: "actors",
                column: "MoviesActorsId");

            migrationBuilder.CreateIndex(
                name: "IX_movies_GenreId",
                table: "movies",
                column: "GenreId");

            migrationBuilder.CreateIndex(
                name: "IX_movies_NationalityId",
                table: "movies",
                column: "NationalityId");

            migrationBuilder.CreateIndex(
                name: "IX_participants_NationalityId",
                table: "participants",
                column: "NationalityId");

            migrationBuilder.CreateIndex(
                name: "IX_realisators_RealisatorsId",
                table: "realisators",
                column: "RealisatorsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "actors");

            migrationBuilder.DropTable(
                name: "realisators");

            migrationBuilder.DropTable(
                name: "movies");

            migrationBuilder.DropTable(
                name: "participants");

            migrationBuilder.DropTable(
                name: "genres");

            migrationBuilder.DropTable(
                name: "nations");
        }
    }
}
