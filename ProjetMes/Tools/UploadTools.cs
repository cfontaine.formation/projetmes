﻿using System.IO;
using System.Net;
using System.Security.Policy;

namespace Projet_mes.Tools
{
    public static class UploadTools
    {
        public static string? Upload(IFormFile file, string name)
        {
            string? path = null;
            if (file.Length > 0)
            {
                path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "wwwroot", "image"));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string pathFile = name + "-" + GenUniqueStr() + ".jpg";
                using (var fileStream = new FileStream(Path.Combine(path, pathFile), FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                return pathFile;
            }
            return path;
        }

        private static string GenUniqueStr()
        {
            string str = DateTime.Now.Ticks.ToString();
            if (str.Length > 10)
            {
                return str.Substring(0, 10);
            }
            return str;
        }

        public static void Delete(string name)
        {
            if (File.Exists(name))
            {
                File.Delete(Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "wwwroot", "image", name)));
            }
        }
    }
}
