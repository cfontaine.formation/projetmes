﻿using Microsoft.EntityFrameworkCore;
using Projet_mes.Models;

namespace Projet_mes.Repositories
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
            .HasMany(left => left.Realisators)
            .WithMany(right => right.MoviesRealisators)
            .UsingEntity(join => join.ToTable("realisators"));

            modelBuilder.Entity<Movie>()
            .HasMany(left => left.Actors)
            .WithMany(right => right.MoviesActors)
            .UsingEntity(join => join.ToTable("actors"));
        }

        public DbSet<Movie>? Movies { get; set; }
        public DbSet<Participant>? Paticipants { get; set; }
        public DbSet<Genre>? Genres { get; set; }
        public DbSet<Nation>? Nations { get; set; }

    }



}
