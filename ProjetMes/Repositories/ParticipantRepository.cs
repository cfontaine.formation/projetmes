﻿using Projet_mes.Models;

namespace Projet_mes.Repositories
{
    public class ParticipantRepository : GenericRepository<Participant>
    {
        public ParticipantRepository(MovieDbContext dbContext) : base(dbContext)
        {
        }
    }
}
