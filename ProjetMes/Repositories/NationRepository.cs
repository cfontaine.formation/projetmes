﻿using Projet_mes.Models;
using System.Security.Cryptography.X509Certificates;

namespace Projet_mes.Repositories
{
    public class NationRepository : GenericRepository<Nation>
    {
        public NationRepository(MovieDbContext dbContext) : base(dbContext)
        {

        }
        public Nation FindByName(string name)
        {
           return dbContext.Nations.Where(n => n.Name.Equals(name)).First();
        }
    }
}
