﻿using Projet_mes.Models;

namespace Projet_mes.Repositories
{
    public class GenreRepository : GenericRepository<Genre>
    {
        public GenreRepository(MovieDbContext dbContext) : base(dbContext)
        {
        }

        public Genre FindByName(string name)
        {
            return dbContext.Genres.Where(n => n.Name.Equals(name)).First();
        }
    }
}
