﻿using Projet_mes.Models;
using System.Linq.Expressions;

namespace Projet_mes.Repositories
{
    public interface IGenericRepository<T> where T : AbstractEntity
    {
        void Create(T entity);
        void Update(T entity);
        void Delete(long id);
        IList<T> FindAll();
        IList<T> FindAll(int start=0, int maxByPage=50);
        T FindById(long id);
    //    IList<T> FindBy(Expression<Func<T, bool>> predicateWhere);
    //    int Count(Expression<Func<T, bool>> predicateWhere);
        void Save();
    }
}
