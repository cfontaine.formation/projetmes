﻿using Microsoft.EntityFrameworkCore;
using Projet_mes.Models;
using System.Linq.Expressions;

namespace Projet_mes.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : AbstractEntity
    {

        protected MovieDbContext dbContext;
        protected DbSet<T> dbSet;

        public GenericRepository(MovieDbContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<T>();
        }

        public void Create(T entity)
        {
            dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(long id)
        {
            var t = FindById(id);
            if (dbContext.Entry(t).State == EntityState.Detached)
            {
                dbSet.Attach(t);
            }
            dbSet.Remove(t);
        }

        public virtual IList<T> FindAll()
        {
            return dbSet.AsNoTracking().ToList();
        }

        public virtual IList<T> FindAll(int start, int maxByPage)
        {
            return dbSet.AsNoTracking()
                        .Skip(start).Take(maxByPage).ToList();
        }

        public virtual T FindById(long id)
        {
            return dbSet.Find(id);
        }

        //IList<T> IGenericRepository<T>.FindBy(Expression<Func<T, bool>> predicateWhere)
        //{
        //    IQueryable<T> req = dbSet.AsNoTracking();
        //    if (predicateWhere != null)
        //        req = req.Where(predicateWhere);

        //    return req.ToList();
        //}

        //public int Count(Expression<Func<T, bool>> predicateWhere)
        //{
        //    IQueryable<T> req = dbSet.AsNoTracking();
        //    if (predicateWhere != null)
        //        req = req.Where(predicateWhere);

        //    return req.Count();
        //}

        public void Save()
        {
            dbContext.SaveChanges();
        }


    }
}
