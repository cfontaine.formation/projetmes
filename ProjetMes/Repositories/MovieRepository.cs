﻿using Microsoft.EntityFrameworkCore;
using Projet_mes.Models;

namespace Projet_mes.Repositories
{
    public class MovieRepository : GenericRepository<Movie>
    {
        public MovieRepository(MovieDbContext dbContext) : base(dbContext)
        {
        }

		public override IList<Movie> FindAll()
		{
			return dbSet.AsNoTracking()
				.Include(n => n.Nationality)
				.Include(g => g.Genre).ToList();
		}

        public override Movie FindById(long id)
        {
            return dbContext.Movies
                .Include(n => n.Nationality)
                .Include(g => g.Genre)
                .Include(r => r.Realisators)
                .Include(a=> a.Actors)
                .FirstOrDefault(x => x.Id == id);
            
        }
    }
}
