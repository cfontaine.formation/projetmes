﻿using Microsoft.AspNetCore.Mvc;
using Projet_mes.Models;
using Projet_mes.Repositories;
using System.Collections.Generic;
using System.Diagnostics;

namespace Projet_mes.Controllers
{
    public class HomeController : Controller
    {
        private readonly MovieDbContext _context;
        private readonly MovieRepository _movieRepository;

        public HomeController(MovieDbContext context)
        {
            _context = context;
            _movieRepository = new MovieRepository(_context);
        }

        public IActionResult Index()
        {
            IList<Movie> lstMovies = _movieRepository.FindAll();
            return View(lstMovies);
        }

        public IActionResult Detail(long id)
        {
            return View(_movieRepository.FindById(id));
		}
		public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}