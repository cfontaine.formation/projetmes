﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projet_mes.ControlViews;
using Projet_mes.Models;
using Projet_mes.Repositories;
using Projet_mes.Tools;
using System.Drawing;


namespace Projet_mes.Controllers
{
    public class MovieController : Controller
    {
        private readonly MovieDbContext _dbContext;
        private MovieRepository _movieRepository;
        private NationRepository _nationRepository;
        private GenreRepository _genreRepository;

        public MovieController(MovieDbContext context)
        {
            _dbContext = context;
            _movieRepository = new MovieRepository(_dbContext);
            _nationRepository = new NationRepository(_dbContext);
            _genreRepository = new GenreRepository(_dbContext);
        }

        public IActionResult Index()
        {
            return View(_movieRepository.FindAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            MovieViewModel mmv = new MovieViewModel();
            mmv.Genres = _genreRepository.FindAll();
            mmv.Nations = _nationRepository.FindAll();
            return View("CreateEdit", mmv);
        }

        [HttpPost]
        public IActionResult Create(MovieViewModel mmv)
        {
            if (ModelState.IsValid)
            {
                return View("CreateEdit", ModelState);
            }
            mmv.Movie.MoviePoster=UploadTools.Upload(mmv.Poster,mmv.Movie.TitleVf);
            mmv.Movie.Nationality=_nationRepository.FindByName(mmv.Movie.Nationality.Name);
            mmv.Movie.Genre = _genreRepository.FindByName(mmv.Movie.Genre.Name);
            _movieRepository.Create(mmv.Movie);
            _movieRepository.Save();
            return Redirect("~/Movie");
        }
    
        public IActionResult Delete(long id)
        {
            Movie movie= _movieRepository.FindById(id);
            UploadTools.Delete(movie.MoviePoster);
            _movieRepository.Delete(id);
            _movieRepository.Save();
            return Redirect("~/Movie");
        }
    
    }
}
